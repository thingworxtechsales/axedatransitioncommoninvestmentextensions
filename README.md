# Common Investment Modules

Example modules for use in ThingWorx for Axeda Transition customers

## Layout
- Common_Investment_Alarm_Management - Alarm Management Module: converts Axeda Alarms to TWX
- Common_Investment_Audit - Audit Module: Allows for viewing Audit logs
- Common_Investment_AutoRegistration - Auto Registration for Axeda (eMessage) devices
- Common_Investment_ConsoleFramework - Console Framework Module: Basic UI Framework for CI Modules
- Common_Investment_Core - Core Module: Required by all other Modules
- Common_Investment_Data_Purge - Data Purge Module: cleanup for CI Modules, TWX logs, audit logs
- Common_Investment_DeveloperToolkit - Development Tools for creating releases (DEPRECATED)
- Common_Investment_DeveloperToolkit_ExtensionTerminator - "uninstall" TWX Extensions
- Common_Investment_File_Management - File Management Module: Axeda File Management
- Common_Investment_Recent_Assets - Recent Assets Module: plugin for Thing Search and Listing
- Common_Investment_Remote_Access - Remote Access Module: Axeda specific
- Common_Investment_SourceControlEntitiesInstaller - Helper Module to install Source Control Entities
- Common_Investment_ThingSearchAndListing - Thing Search and Listing Module: Default asset search
- dist - files for releases
- EXAMPLE-GasPinning - Example code for GAS Pinning
- EXAMPLE-ProvisioningExtension - Example code for Provisioning
- scripts - various helper scripts most used for CI
- src - files for development
- bitbucket-pipelines.yml - used for CI in Bitbucket

## Installation
Instructions are in each indivudual folder or the ZIP files under dist and src folders