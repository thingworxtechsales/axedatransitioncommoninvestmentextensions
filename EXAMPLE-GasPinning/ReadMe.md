This was put together based on services delivered work to Bayer.  That was a bit of a mess, so in trying to understand what they were doing, I basically just cleaned up their code and UI.  This is just an export of those entities.  This is not an extension and should not be passed off as supported extension.  At a high level, it requires you to add the supplied TS to the GAS entities.  This enabled associated thing/region/country capabilities.  In addition, on the GASProvider, you would need to update the selector property to point to the supplied pointer.  We can't change the provider as sessions don't get established when that is changed.

UI is pretty straight forward.  Enables some of the functionality from the original EMT entities plus allows you to associate a GAS server to assets/regions/countries.  Given regions and countries may be at different layers in a customer's organization, you may need to modify the GetAllCountries and GetAllRegions services accordingly.

Let me know if you have any issues.

Jake