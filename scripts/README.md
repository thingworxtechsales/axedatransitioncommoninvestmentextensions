# Scripts

## Prerequisites

- Python 3


## Usage
usage: package_entities.py [-h] [--file FILE] [--out OUT] [--zipall] [--zipfile ZIPFILE] [--clean] basepath

Script to package CI Module directories into ThingWorx Source Control ZIP packages.

positional arguments:

    basepath              The full path to the Module directories.

optional arguments:

    -h, --help            show this help message and exit
    --file FILE, -f FILE  The full path to the modules.json config file.
    --out OUT, -o OUT     The output path for created Module packages. Defaults to "package" in the current directory.
    --zipall, -z          Create a ZIP of all packages in outpath.
    --zipfile ZIPFILE     The ZIP file to create for the CI Modules.
    --clean, -c           Remove all files from out path before creating packages.


### Examples

Create Source Control Entities ZIPs for Modules.

```python package_entities.py --file <module.json file> <path to root of Modules>```

i.e.

    python .\package_entities.py --file .\modules.json ..\


This will scan the folder above scripts for the modules defined in `scripts\modules.json` and output the created ZIPs to `scripts\package` (default out path).  Each module may have up to 2 packages created - one for the Modules itself and another for any Extension dependencies.


Create Create Source Control Entities ZIPs for Modules and ZIP all Modules.

```python package_entities.py --file <module.json file> <path to root of Modules> --zipall```

i.e.

    python .\package_entities.py --file .\modules.json ..\ --zipall

This will perform the same actions as above but will create a ZIP with all the Modules that were created instead of creating separate folders.  The ZIP file will be placed in the --out folder (default ```scripts/package```).  The file by default will be called CI_<timestamp>.zip but this can be overridden by using the ```--zipfile``` parameter with the full path to the file to create.


#### 'outpath' directory format:

    <Module name>
     - <Module name>-<version>-Entities.zip
     - <Module name>-<version>-ExtensionDeps.zip
     - Extension
        - <Existing Module Extension files>

i.e. for Core

    Common_Investment_Core
     - Extension
       - Common_Investment_Core-2.2.5-EXTENSION.zip
       - Common_Investment_Core-2.2.5-EXTENSION-STANDALONE.zip
       - Common_Investment_Core-2.2.5-SOURCECONTROL.zip
    - Common_Investment_Core-2.2.5-documentation.docx
    - Common_Investment_Core-2.2.5-Entities.zip
    - Common_Investment_Core-2.2.5-ExtensionDeps.zip



## Configuration
Uses modules.json config file which uses the following format:

    {
        "config": {
            "disclaimer": {
                "exclude": [ // List of ThingWorx Entity types to exclude from adding disclaimer text
                    "Mashup"
                ]
            }
        },
        "modules": [
            {
                "dir": "Common_Investment_Core/ThingWorx",
                "name": "Common_Investment_Core",
                "extension": [
                    "Entities/**/*",
                    "metadata.xml"
                ],
                "ui": [
                    "Core_UI/**/*"
                ],
                "deps": [
                    "FilePondWidget.zip",
                    "PostgreSQLConnector_Extension.zip"
                ],
                "docs": [
                    "Common_Investment_Core*.docx"
                ],
                "packages": [
                    "Common_Investment_Core*.zip"
                ]
            },
            ... // other modules here
        ],
        "source": [  // no changes will be made - files will jsut be copied as part of packaging
            {
                "dir": "EXAMPLE-GasPinning",
                "name": "EXAMPLE-GasPinning"
            },
            ... // other source directories here
        ]
    }

Key values:
* dir: The full or relative path to the Module root directory.  
    This should contain the existing Extension ZIP, Source Control Entities, and documentation
* name: The name of the Module.  Usually just the Module directory name.
* extension: A list of glob patterns for files that make up the Extension (i.e. EXTENSION entities)
* ui (optional): A list of glob patterns for the files that make up the UI ENtities (i.e. SOURCECONTROL entities)
* deps (optional): A list of Extension Packages that the Module depends on.
    These will be packaged in a separate ZIP file from the other Entities to allow for installation.
* docs: A list of glob patterns for the files that make up the Documentation.
* packages: A list of the current ZIP files that make up the Module packages.  
    (i.e. *EXTENSION.zip, *SOURCECONTROL.zip, *STANDALONE.zip)
