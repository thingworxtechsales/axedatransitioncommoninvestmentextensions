"""
Script to package CI Module directories into ThingWorx Source Control ZIP packages.

This will place all created packages into the specified 'outpath'.  Existing Extension packages
will be placed into an 'Extension' folder under 'outpath'.

'outpath' directory format:
<Module name>
 - <Module name>-<version>-Entities.zip
 - <Module name>-<version>-ExtensionDeps.zip
 - <Module name>-<version>-BuildingBlock.zip

Uses modules.json config file which uses the following format:

{
    "config": {
		"disclaimer": {
			"exclude": [
				"Group"
			]
		}	
	},
	"modules": [
		{
			"dir": "Common_Investment_Audit",
			"name": "Common_Investment_Audit",
			"thingworx": {
				"path": "Thingworx",
				"packageZipDir": "PTC.CI.Audit",
				"versionFile": true,
				"extension": [
					"Entities/**/*"
				]
			},
			"buildingBlock": {
				"path": "BuildingBlock",
				"packageSuffix": "-BuildingBlock",
				"packageZipDir": "PTC.CI.BB.Audit",
				"versionFile": true,
				"extension": [
					"PTC.CI.BB.Audit/**/*"
				]
			},
			"docs": [
				"Thingworx/Common_Investment_Audit*.docx"
			]
		},
        ...
    ],
    "source": [  // just copy as-is
        {
            "dir": "EXAMPLE-GasPinning",
            "name": "EXAMPLE-GasPinning"
        },
        ...
    ]
}

Key values:
* dir: The full or relative path to the Module root directory.  
    This should contain the existing Extension ZIP, Source Control Entities, and documentation
* name: The name of the Module.  Usually just the Module directory name.
* extension: A list of glob patterns for files that make up the Extension (i.e. EXTENSION entities)
* ui (optional): A list of glob patterns for the files that make up the UI ENtities (i.e. SOURCECONTROL entities)
* deps (optional): A list of Extension Packages that the Module depends on.
    These will be packaged in a separate ZIP file from the other Entities to allow for installation.
* docs: A list of glob patterns for the files that make up the Documentation.
* packages: A list of the current ZIP files that make up the Module packages.  
    (i.e. *EXTENSION.zip, *SOURCECONTROL.zip, *STANDALONE.zip)

"""

import add_disclaimer

import argparse
import os
import json
import glob
import pathlib
import re
import shutil
from datetime import datetime
import zlib
import zipfile
import tempfile


"""
Load the specified JSON config file
"""
def loadConfigFile(file):
    f = open(file, "r")
    data = json.load(f)
    f.close()
    return data


"""
Create all directories in path if they don't exists
"""
def createDirsFromPath(path):
    dir = os.path.dirname(path)
    if not os.path.exists(dir):
        os.makedirs(dir)

"""
Various cleanup and pre-packaging steps
"""
def prepareModule(module, baseDir, exclude):
    # run add-disclaimer on files
    blurb = add_disclaimer.getDisclaimerText(os.path.join(os.path.dirname(__file__), 'disclaimer.txt'))
    add_disclaimer.processModule(module, baseDir, exclude, blurb)


"""
Package an individual Module to outPath
"""
def packageModule(module, baseDir, outPath):
    files = []
    for i in module["extension"]:
        path = f"{baseDir}/{i}"
        #print(path)
        f = glob.glob(path)
        #print(f)
        nf = []
        for fp in f:
            nf.append(fp.replace(os.path.dirname(path), ""))
        files = files + f

    if "ui" in module:
        for j in module["ui"]:
            path = f"{baseDir}/{j}"
            #print(path)
            f = glob.glob(path)
            #print(f)
            nf = []
            for fp in f:
                nf.append(fp.replace(os.path.dirname(path), ""))
            files = files + f

    #print(files)

    useVersionFile = module["versionFile"] if "versionFile" in module else False;
    if useVersionFile:
        version = getVersionFromFile(baseDir)
    else:
        version = getVersionFromMetadata(baseDir)

    if not version:
        version = "0.0.0"
    #version = module["version"]
    if "packageSuffix" in module:
        packageSuffix = module["packageSuffix"]
    else:
        packageSuffix = "-Entities"

    if "packageZipDir" in module:
        packageZipDir = module["packageZipDir"]
    else:
        packageZipDir = module["name"]
        
    outzipfile = f'{outPath}/{module["name"]}/{module["name"]}-{version}{packageSuffix}.zip'
    writeZipFile(packageZipDir, baseDir, files, outzipfile)

    """ if "bb" in module:
        files = []
        bbBaseDir = module["bb"]["path"]
        for j in module["bb"]["files"]:
            path = f"{baseDir}/{bbBaseDir}/{j}"
            #print(path)
            f = glob.glob(path)
            #print(f)
            nf = []
            for fp in f:
                nf.append(fp.replace(os.path.dirname(path), ""))
            files = files + f

        #print(files)

        #version = getVersionFromMetadata(baseDir)
        versionPath = os.path.realpath(f"{baseDir}/{bbBaseDir}")
        version = getVersionFromFile(versionPath)
        if not version:
            version = "0.0.0"
        #version = module["version"]
        outzipfile = f'{outPath}/{module["name"]}/{module["name"]}-{version}-BuildingBlock.zip'
        writeZipFile(module["name"], baseDir, files, outzipfile) """

    if "deps" in module:
        files = []
        for d in module["deps"]:
            path = f"{baseDir}/{d}"
            #print(path)
            f = glob.glob(path)
            #print(f)
            nf = []
            for fp in d:
                nf.append(fp.replace(os.path.dirname(path), ""))
            files = files + f

        outzipfile = f'{outPath}/{module["name"]}/{module["name"]}-{version}-ExtensionDeps.zip'
        writeDependenciesToZipFile(module["name"], baseDir, files, outzipfile)

"""
Package a source directory
"""
def packageSource(source, baseDir, outPath):
    files = []
    path = f"{baseDir}/**/*"
    #print(path)
    files = glob.glob(path, recursive=True)
    #print(f)

    path = f'{os.path.normpath(outPath)}/{source["name"]}/'
    #print(module["name"] + ": " + path)
    for file in files:
        if os.path.isfile(file):
            #print(file)
            newfile = file.replace(baseDir, "")
            newPath = os.path.normpath(path + os.path.sep + newfile)
            createDirsFromPath(newPath)
            shutil.copy2(file, newPath)


"""
Package all Modules to outPath
"""
def packageAllModules(basePath, outPath, config, skipPreSteps = False):
    #
    exclude = config["config"]["disclaimer"]["exclude"]
    for module in config["modules"]:
        #print(module)
        baseDir = f'{basePath}/{module["dir"]}'
        with tempfile.TemporaryDirectory() as tmpDir:
            tmpPath = f'{tmpDir}/{module["dir"]}'
            shutil.copytree(baseDir, tmpPath)
            if ("thingworx" in module or "buildingBlock" in module):
                subs = ["thingworx", "buildingBlock"]
                for sub in subs:
                    # skip if it doesn't exist in module config
                    if not sub in module:
                        continue
                    subModule = module[sub]
                    subModule["name"] = module["name"]
                    subPath = subModule["path"]
                    subTmpPath = f"{tmpPath}/{subPath}"
                    if skipPreSteps == False:
                        prepareModule(subModule, subTmpPath, exclude)
                    packageModule(subModule, subTmpPath, outPath)
            else:
                if skipPreSteps == False:
                    prepareModule(module, tmpPath, exclude)
                packageModule(module, tmpPath, outPath)
        
    for source in config["source"]:
        baseDir = f'{basePath}/{source["dir"]}'
        packageSource(source, baseDir, outPath)

"""
Add all extension dependency files to outzipfile
"""
def writeDependenciesToZipFile(name, baseDir, files, outzipfile):
    createDirsFromPath(outzipfile)
    
    baseDir = os.path.normpath(baseDir) + os.path.sep
    with zipfile.ZipFile(outzipfile,'w', compression=zipfile.ZIP_DEFLATED) as zip:
        for file in files:
            file = os.path.normpath(file)
            #print("file: " + file)
            if not os.path.exists(file):
                print("ERROR: File not found - " + file)
                continue
            path = file.replace(baseDir, "")

            zip.write(file, path)


"""
Adds all files for the module to outzipfile
"""
def writeZipFile(name, baseDir, files, outzipfile):
    # create folders if needed
    #print(outzipfile)
    createDirsFromPath(outzipfile)

    baseDir = os.path.normpath(baseDir) + os.path.sep
    #print(name + ": " + baseDir)
    with zipfile.ZipFile(outzipfile,'w', compression=zipfile.ZIP_DEFLATED) as zip:
        # writing each file one by one
        for file in files:
            # set up variables needed
            file = os.path.normpath(file)
            #print("file: " + file)
            if not os.path.exists(file):
                print("ERROR: File not found - " + file)
                continue
            path = file.replace(baseDir, "")
            p = pathlib.Path(path)

            # grab the first directory
            dir = pathlib.Path(*p.parts[:1])

            # ignore metadata.xml - not needed with SC Entities
            if p.name == 'metadata.xml':
                continue
            
            # don't replace dir name for files under base dir
            elif pathlib.Path(baseDir + str(dir)).is_file():
                print("ignored: " + baseDir + str(dir))
                path
                # no op

            # add anything else
            else:
                path = path.replace(str(dir), name, 1)
            zip.write(file, path)


"""
Gets the version from the Module metadata.xml file

- expects metadata.xml to have a packageVersion property
- returns False if it can't find a version
"""
def getVersionFromMetadata(path):
    metafile = f'{path}/metadata.xml'
    if not os.path.exists(metafile):
        print("ERROR: File not found - " + metafile)
        return False
    
    with open(metafile, "r") as infile:
        data = infile.read()

    infile.close()
    #print("data: " + data)
    pattern = re.compile("packageVersion=\"(.*?)\"", re.MULTILINE)
    match = pattern.search(data)
    version = match.group(1)
    #print("match: " + version)
    if version:
        return version
    else:
        return False


"""
Gets the version from the VERSION file

- returns False if it can't find a version
"""
def getVersionFromFile(path):
    file = f'{path}/VERSION'
    if not os.path.exists(file):
        print("ERROR: File not found - " + file)
        return False
    
    with open(file, "r") as infile:
        data = infile.readline()

    infile.close()
    #print("data: " + data)
    version = data.strip()
    #print("match: " + version)
    if version:
        return version
    else:
        return False


"""
Copy all Extension Packages and Documentation to outPath

- creates 'Extension' folder under outPath/<module>
"""
def copyModuleFiles(basePath, outPath, config):
    # copy files from each Module
    for module in config["modules"]:
        #print(module)
        baseDir = f'{basePath}/{module["dir"]}'

        # Copy Extension Packages
        # files = []
        # if "packages" in module:
        #     for p in module["packages"]:
        #         path = os.path.normpath(f"{baseDir}/{p}")
        #         f = glob.glob(path)
        #         files = files + f
        # else:
        #     print(module["name"] + ": Missing Packages")

        # path = f'{os.path.normpath(outPath)}/{module["name"]}/Extension/'
        # #print(module["name"] + ": " + path)
        # for file in files:
        #     #print(file)
        #     createDirsFromPath(path)
        #     shutil.copy2(file, path)

        # Copy Documentation
        files = []
        if "docs" in module:
            for d in module["docs"]:
                path = os.path.normpath(f"{baseDir}/{d}")
                f = glob.glob(path)
                files = files + f
        else:
            print(module["name"] + ": Missing Docs")

        path = f'{os.path.normpath(outPath)}/{module["name"]}'
        #print(module["name"] + ": " + path)
        for file in files:
            #print(file)
            createDirsFromPath(path)
            shutil.copy2(file, path)


"""
Clean the outPath of all files and directories

- Removes outPath and recreates
"""
def cleanOutPath(outPath):
    if os.path.exists(outPath):
        shutil.rmtree(outPath)
        createDirsFromPath(outPath)


"""
Create a ZIP Package of all packages in outPath
"""
def createAllPackagesZip(outPath, outzipfile):
    dirPattern = f'{outPath}/*'
    dirs = glob.glob(dirPattern)
    cleanPath = outPath

    files = []
    for dir in dirs:
        pattern = f'{dir}/**/*'
        f = glob.glob(pattern, recursive=True)
        files = files + f

    with tempfile.TemporaryDirectory() as tmpDir:          
        tmpZipFile = f'{tmpDir}/all.zip'
        with zipfile.ZipFile(tmpZipFile,'w', compression=zipfile.ZIP_DEFLATED) as zip:
            # writing each file one by one

            outPath = os.path.normpath(outPath)
            #print (outPath)
            for file in files:
                file = os.path.normpath(file)
                #print("file: " + file)
                path = file.replace(outPath, "")

                zip.write(file, path)
        
        # clean modules dir
        cleanOutPath(cleanPath)
        createDirsFromPath(outzipfile)
        
        # copy tmpZipFile to outzipfile
        shutil.copy2(tmpZipFile, outzipfile)


"""
Main function

Called by default when script is executed
"""
def main():
    
    # setup argparse
    parser = argparse.ArgumentParser(description="Script to package CI Module directories into ThingWorx Source Control ZIP packages.")
    parser.add_argument('basepath', help='The full path to the Module directories.')
    parser.add_argument('--file','-f', help='The full path to the modules.json config file.')
    parser.add_argument('--out','-o', default="./dist", help='The output path for created Module packages.  Defaults to "dist" in the root directory.')	
    parser.add_argument('--zipall','-z', action="store_true", help='Create a ZIP of all packages in outpath.')	
    parser.add_argument('--zipfile', dest="outzipfile", help='The ZIP file to create for the CI Modules.')	
    parser.add_argument('--clean','-c', action="store_true", help='Remove all files from out path before creating packages.')	
    parser.add_argument('--skip-pre-steps', action="store_true", help='Skip pre-processing steps.')	

    # parse the args
    args = parser.parse_args()
    file = args.file
    basePath = os.path.normpath(args.basepath)
    outPath = args.out
    clean = args.clean
    zipAll = args.zipall
    outzipfile = args.outzipfile
    skipPreSteps = args.skip_pre_steps
    #deployPath = args.deploypath

    if clean:
        cleanOutPath(outPath)

    # do the actual work
    config = loadConfigFile(file)
    packageAllModules(basePath, outPath, config, skipPreSteps)
    copyModuleFiles(basePath, outPath, config)
    
    if zipAll:
        if not outzipfile:
            outzipfile = f'{outPath}/CI_{datetime.now().strftime("%Y-%m-%dT%H%M%S")}.zip'
        createAllPackagesZip(outPath, outzipfile)

if __name__ == "__main__": 

    # calling main function 
    main() 
