#!/bin/bash
SOURCE_PATH=$1

if [ -z $SOURCE_PATH ]; then
    echo "Usage: $0 SOURCE_PATH"
    echo "SOURCE_PATH is required"
    exit 1
fi

# create src packages
echo "Creating src packages"
python $SOURCE_PATH/scripts/package_entities.py --file $SOURCE_PATH/scripts/modules.json --out $SOURCE_PATH/src --clean $SOURCE_PATH/ --skip-pre-steps

# create dist packages
echo "Creating dist packages"
python $SOURCE_PATH/scripts/package_entities.py --file $SOURCE_PATH/scripts/modules.json --out $SOURCE_PATH/dist --clean $SOURCE_PATH/ --zipall