@ECHO ON
set SOURCE_PATH=%1

::create src packages
ECHO Creating src packages
python %SOURCE_PATH%/scripts/package_entities.py --file %SOURCE_PATH%/scripts/modules.json --out %SOURCE_PATH%/src --clean %SOURCE_PATH%/ --skip-pre-steps

::create dist packages
ECHO Creating dist packages
python %SOURCE_PATH%/scripts/package_entities.py --file %SOURCE_PATH%/scripts/modules.json --out %SOURCE_PATH%/dist --clean %SOURCE_PATH%/ --zipall
