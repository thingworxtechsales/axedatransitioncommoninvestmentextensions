"""
Script to add legal disclaimer to all XML files.

This will add the disclaimer in disclaimer.txt to the Entity Description and all Services Code in those Entities.

"""

import argparse
import os
import json
import glob
from xml.etree import ElementTree as et
import pathlib
import re
import shutil
from datetime import datetime

TEXT_DESCRIPTION_PRE = "PTC DISCLAIMER: \n"
TEXT_DESCRIPTION_POST = "\nEND DISCLAIMER\n\n"
TEXT_SERVICE_PRE = "/** START PTC DISCLAIMER"
TEXT_SERVICE_POST = "END PTC DISCLAIMER **/\n\n"

"""
Load the specified JSON config file
"""
def loadConfigFile(file):
    f = open(file, "r")
    data = json.load(f)
    f.close()
    return data


"""
Process all Modules
"""
def processAllModules(config, basePath, exclude, blurb, strip = False):  
    for module in config["modules"]:
        #print(module)
        rootDir = f'{basePath}/{module["dir"]}'
        for fileType in {"thingworx", "buildingBlock"}:
            if fileType in module:
                baseDir = f'{rootDir}/{module[fileType]["path"]}'
                processModule(module[fileType], baseDir, exclude, blurb, strip)


"""
Add disclaimer to an individual Module's files
"""
def processModule(module, baseDir, exclude, blurb, strip = False):
    files = []
    for i in module["extension"]:
        path = f"{baseDir}/{i}.xml"
        #print(path)
        f = glob.glob(path, recursive = True)
        #print(f)
        nf = []
        for fp in f:
            if not isFileExcluded(fp, exclude):
                nf.append(fp.replace(os.path.dirname(path), ""))
        files = files + nf

    if "ui" in module:
        for j in module["ui"]:
            path = f"{baseDir}/{j}.xml"
            #print(path)
            f = glob.glob(path, recursive = True)
            #print(f)
            nf = []
            for fp in f:
                if not isFileExcluded(fp, exclude):
                    nf.append(fp.replace(os.path.dirname(path), ""))
            files = files + nf

    #print(files)

    for file in files:
        # with open(file, "w+") as f:
        with open(file, "r+") as f:
            content = f.read()
            if content:
                if strip == True:
                    content = removeDisclaimerFromDescription(content)
                    content = removeDisclaimerFromServices(content)
                else:
                    content = addDisclaimerToDescription(blurb, content)
                    content = addDisclaimerToServices(blurb, content)

                # print(content)
                f.seek(0)
                f.write(content)
                f.truncate()

        # tree = et.parse(file)
        # tree = addDisclaimerToDescription(tree, blurb, file)
        # tree = addDisclaimerToServices(tree, blurb, file)
        # tree.write(datafile)


"""
Determine if this file should be excluded
"""
def isFileExcluded(file, exclude):
    found = False
    if exclude:
        for x in exclude:
            pattern = re.compile(r'<' + re.escape(x) + 's>.*?<' + re.escape(x), re.DOTALL)

            lines = ''
            cnt = 50
            for i, line in enumerate(open(file)):
                if i > cnt:
                    break
                lines += line

            match = re.search(pattern, lines)
            if match:
                found = True
                break

    return found


"""
Get the Disclaimer Text from the file
"""
def getDisclaimerText(disclaimerFile):
    with open(disclaimerFile) as f:
        blurb = f.read()
    return blurb


"""
Add the Disclaimer text to the entity description in the XML file
"""
def addDisclaimerToDescription(blurb, content):

    replace = r'description="' + TEXT_DESCRIPTION_PRE + blurb + TEXT_DESCRIPTION_POST + '\\1"'

    # check if we already have added the blurb and just replace the blurb content
    pattern = re.compile(r'description="' + re.escape(TEXT_DESCRIPTION_PRE) + '.*?' + re.escape(TEXT_DESCRIPTION_POST) + '(.*?)"', re.DOTALL)
    content,cnt = pattern.subn(replace, content, 1)

    if (cnt == 0):
        # find only the first instance of the description attribute
        pattern = re.compile(r'description="(.*?)"', re.DOTALL)
        content = pattern.sub(replace, content, 1)

    return content


"""
Add the Disclaimer text to all Services in the entity in the XML file
"""
def addDisclaimerToServices(blurb, content):

    replace = r'<code>\1<![CDATA[\1' + TEXT_SERVICE_PRE + '\\1' + blurb + '\\1' + TEXT_SERVICE_POST + '\\2]]>\\1</code>'

    # check if we already have added the blurb and just replace the blurb content
    pattern = re.compile(r'<code>(.*?)<\!\[CDATA\[.*?' + re.escape(TEXT_SERVICE_PRE) + '.*?' + re.escape(TEXT_SERVICE_POST) + '(.*?)\]\]>.*?<\/code>', re.DOTALL)
    content,cnt = pattern.subn(replace, content)
    
    if (cnt == 0):
        # find only the first instance of the description attribute
        pattern = re.compile(r'<code>(.*?)<\!\[CDATA\[(.*?)\]\]>.*?</code>', re.DOTALL)
        content,cnt = pattern.subn(replace, content)

    return content


"""
Remove the Disclaimer text from entity description in the XML file
"""
def removeDisclaimerFromDescription(content):
    
    blurb = ""
    replace = r'description="\1"'

    # check if we already have added the blurb and just replace the blurb content
    pattern = re.compile(r'description="' + re.escape(TEXT_DESCRIPTION_PRE) + '.*?' + re.escape(TEXT_DESCRIPTION_POST) + '(.*?)"', re.DOTALL)
    content,cnt = pattern.subn(replace, content)
    #print("removeDisclaimerFromDescription: " + str(cnt))
    return content


"""
Remove the Disclaimer text from all Services in the entity in the XML file
"""
def removeDisclaimerFromServices(content):
    
    blurb = ""
    replace = r'<code>\1<![CDATA[\\1\\2]]>\1</code>'

    # check if we already have added the blurb and just replace the blurb content
    pattern = re.compile(r'<code>(.*?)<\!\[CDATA\[.*?' + re.escape(TEXT_SERVICE_PRE) + '.*?' + re.escape(TEXT_SERVICE_POST) + '(.*?)\]\]>.*?<\/code>', re.DOTALL)
    content,cnt = pattern.subn(replace, content)
    #print("removeDisclaimerFromServices: " + str(cnt))
    return content


"""
Called by default when script is executed
"""
def main():
    
    # setup argparse
    parser = argparse.ArgumentParser(description="Script to package CI Module directories into ThingWorx Source Control ZIP packages.")
    parser.add_argument('basepath', help='The full path to the Module directories.')
    parser.add_argument('--file','-f', default="./disclaimer.txt", help='The full path to the disclaimer.txt file.')
    parser.add_argument('--config','-c', help='The full path to the modules.json config file.')
    parser.add_argument('--exclude', '-x', action='append', help='Exclude Entity Types')
    parser.add_argument('--strip', '-s', action="store_true", help='Strip Existing Disclaimer')

    # parse the args
    args = parser.parse_args()
    file = args.file
    basePath = os.path.normpath(args.basepath)
    exclude = args.exclude
    strip = args.strip
    
    # do the actual work
    config = loadConfigFile(args.config)
    blurb = getDisclaimerText(file)
    processAllModules(config, basePath, exclude, blurb, strip)
    
if __name__ == "__main__": 

    # calling main function 
    main() 
